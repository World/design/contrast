use gettextrs::*;
use gtk::{gio, glib};

mod application;
mod colour;
mod colour_entry;
#[rustfmt::skip]
mod config;
mod contrast_level;
mod contrast_preview;
mod level_bar;
mod window;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

fn main() -> glib::ExitCode {
    // Initialize logger, debug is carried out via debug!, info!, and warn!.
    tracing_subscriber::fmt::init();

    gtk::init().expect("Unable to start GTK");
    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).unwrap();
    textdomain(GETTEXT_PACKAGE).unwrap();

    glib::set_application_name(&gettext("Contrast"));

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/contrast.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    Application::run()
}
