use gettextrs::gettext;
use gtk::{glib, prelude::*, subclass::prelude::*};

mod imp {
    use std::cell::Cell;

    use super::*;

    #[derive(Default, glib::Properties, gtk::CompositeTemplate)]
    #[properties(wrapper_type = super::ContrastPreview)]
    #[template(resource = "/org/gnome/design/Contrast/contrast_preview.ui")]
    pub struct ContrastPreview {
        #[template_child]
        pub head_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub body_label: TemplateChild<gtk::Label>,
        #[property(get, set = Self::set_contrast_level, minimum = 0.0, maximum = 21.0, default = 0.0)]
        pub contrast_level: Cell<f32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContrastPreview {
        const NAME: &'static str = "ContrastPreview";
        type ParentType = gtk::Widget;
        type Type = super::ContrastPreview;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ContrastPreview {
        fn dispose(&self) {
            self.dispose_template();
        }
    }

    impl WidgetImpl for ContrastPreview {}

    impl ContrastPreview {
        fn set_contrast_level(&self, contrast_level: f32) {
            if (0.0..3.0).contains(&contrast_level) {
                self.head_label.set_text(&gettext("Nope"));
                self.body_label.set_text(&gettext(
                    "This color combination doesn’t have enough contrast to be legible.",
                ));
            } else if (3.0..4.5).contains(&contrast_level) {
                self.head_label.set_text(&gettext("Not bad"));
                self.body_label.set_text(&gettext(
                    "This color combination can work, but only at large text sizes.",
                ));
            } else if (4.5..7.0).contains(&contrast_level) {
                self.head_label.set_text(&gettext("Pretty good"));
                self.body_label.set_text(&gettext(
                    "This color combination should work OK in most cases.",
                ));
            } else if (7.0..=21.0).contains(&contrast_level) {
                self.head_label.set_text(&gettext("Awesome"));
                self.body_label
                    .set_text(&gettext("This color combination has great contrast."));
            }
            self.contrast_level.set(contrast_level);
        }
    }
}

glib::wrapper! {
    pub struct ContrastPreview(ObjectSubclass<imp::ContrastPreview>)
        @extends gtk::Widget;
}
